#pragma checksum "D:\WoRk\iLink\CheckOut\ForntEnd\FrontEnd.Web\FrontEnd.Web\Views\CheckOut\Error.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9644a55ef3596130b0dfd48b77d4498530a20c40"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_CheckOut_Error), @"mvc.1.0.view", @"/Views/CheckOut/Error.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\WoRk\iLink\CheckOut\ForntEnd\FrontEnd.Web\FrontEnd.Web\Views\_ViewImports.cshtml"
using FrontEnd.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\WoRk\iLink\CheckOut\ForntEnd\FrontEnd.Web\FrontEnd.Web\Views\_ViewImports.cshtml"
using FrontEnd.Web.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9644a55ef3596130b0dfd48b77d4498530a20c40", @"/Views/CheckOut/Error.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"15d64e4ed31d6e7e32094d16927043cf9e760ddc", @"/Views/_ViewImports.cshtml")]
    public class Views_CheckOut_Error : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div class=""col-10 offset-1 col-md-6 offset-md-3 col-lg-4 offset-lg-4 box-shadow-2"">
    <div class=""card border-grey border-lighten-3 px-2 my-0 row"">
        <div class=""card-header no-border pb-1"">
            <div class=""card-body"">
                <h2 class=""error-code text-center mb-2 text-danger"">404</h2>
                <h3 class=""text-uppercase text-center"">ไม่พบข้อมูลลูกค้า !</h3>
            </div>
        </div>
        <div class=""card-content px-2"">
            <fieldset class=""row py-1"">
                <div class=""input-group col-12"">
                    <input type=""text"" class=""form-control border-grey border-lighten-1"" id=""searchText"" placeholder=""ค้นหารหัสลูกค้า..."" aria-describedby=""button-addon2"">
                    <span class=""input-group-append"" id=""button-addon2"">
                        <button class=""btn btn-secondary border-grey border-lighten-1"" type=""button"" onclick=""goSearch()""><i class=""la la-search""></i></button>
                    </span>
                </di");
            WriteLiteral("v>\r\n            </fieldset>\r\n");
            WriteLiteral("        </div>\r\n        \r\n    </div>\r\n</div>\r\n\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral(@"

    <script>
        function goSearch() {
            let data = $(""#searchText"").val();
            if (data == """") {
                toastr.error(""กรุณากรอกข้อมูลรหัสลูกค้า เพื่อค้นหา"");
                return false;
            }

            $.ajax({
                cache: false,
                async: true,
                type: ""POST"",
                url: """);
#nullable restore
#line 42 "D:\WoRk\iLink\CheckOut\ForntEnd\FrontEnd.Web\FrontEnd.Web\Views\CheckOut\Error.cshtml"
                 Write(Url.Action("Searching", "CheckOut"));

#line default
#line hidden
#nullable disable
                WriteLiteral(@""",
                data: { customer_no: data },
                success: function (data) {
                    //debugger
                    if (data.status == ""success"") {
                        debugger
                        //toastr.success(""Completed"");
                        window.location = ""/CheckOut/Index?customer="" + data.message
                    } else {
                        toastr.error(""Fail "" + data.message);
                        swal.close();
                    }
                },
                error: function (err) {
                    toastr.error(err);
                },
                complete: function (data) {
                }
            });
        }
    </script>

");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
