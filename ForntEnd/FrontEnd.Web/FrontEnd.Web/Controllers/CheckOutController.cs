﻿using Dapper;
using FrontEnd.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Web.Controllers
{
    public class CheckOutController : Controller
    {
        private IConfiguration Configuration;
        IDatabaseConnectionFactory _db;
        private readonly ILogger<CheckOutController> _logger; private string _apiKey = "";
        public CheckOutController(ILogger<CheckOutController> logger, IConfiguration _configuration, IDatabaseConnectionFactory _database)
        {
            _logger = logger;
            Configuration = _configuration;
            _db = _database;
        }
        private List<InvoiceData> _listInvoice(string customer)
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @$"Select * from tran_Invoice WHERE   CustomerNo = '{customer}' AND (IsActive = 1) AND (IsPaid = 0) Order BY Id DESC";
            var _inv = conn.Query<InvoiceData>(query).ToList();
            return _inv;
        }
        private InvoiceData _getInvoice(long Id)
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"Select * from tran_Invoice WHERE (Id = @Id) AND  (IsActive = 1) AND (IsPaid = 0)";
            var _inv = conn.Query<InvoiceData>(query, new { Id }).FirstOrDefault();
            return _inv;
        }
        private bool _paidInvoice(string inv_no)
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"select * from  tran_Invoice  where InvoiceNo = @inv_no AND IsActive = 1 AND IsPaid = 0";
            var _inv = conn.Query<InvoiceData>(query, new { inv_no }).FirstOrDefault();
            if (_inv == null)
            {
                return false;
            }
            else
            {
                //paid
                var insertQuery = @"update tran_Invoice set IsPaid = 1 where InvoiceNo = @inv_no";
                conn.Execute(insertQuery, new { inv_no = inv_no });
                return true;
            }
        }

        private List<InvoiceData> _searching(string custNo)
        {
            var _data = _listInvoice(custNo);
            return _data;
        }
        public IActionResult Index(string customer)
        {
            if (!string.IsNullOrWhiteSpace(customer))
            {
                var model = _listInvoice(customer);

                if (model.Count() > 0)
                {

                ViewBag.AppTitle = "Invoice";
                return View(model);
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            else
            {
                return RedirectToAction("Error");
            }
            
        }
        public IActionResult Error()
        {

            return View();
        }
        public IActionResult Review(long? id)
        {
            var model = _getInvoice(id.Value);
            ViewBag.AppTitle = "Invoice Detail";
            return View(model);
        }
     
        [HttpPost]
        public IActionResult ProcessCheckOut(string mid, string cardid, string token, decimal local_amount, string local_tel, string local_invoice, string local_custno, string local_custname, string local_email, string objectId, string dcc_currency)
        {
            _apiKey = Configuration.GetValue<string>("BaseData:SECKEY");
            var rec = new ReceiveTransactionData()
            {
                mid = mid,
                cardid = cardid,
                secretKey = _apiKey,
                token = token,

                amount = local_amount,
                 email = local_email,
                 tel = local_tel,
                 cust_name = local_custname,
                 cust_no = local_custno,
                 invoice_no = local_invoice,
                 dcc_cur = dcc_currency
            };
            return View(rec);
        }

        [HttpPost]
        public async Task<IActionResult> ProcessCharge(string model, string secret)
        {
            string _apiUrl = Configuration.GetValue<string>("BaseData:APIUrl");
            List<ChargeTransactionData> modeldata = JsonConvert.DeserializeObject<List<ChargeTransactionData>>(model);
            ReponseMessage result = new ReponseMessage();
            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(modeldata[0]), Encoding.UTF8, "application/json");
                httpClient.DefaultRequestHeaders.Add("x-api-key", secret);

                using (var response = await httpClient.PostAsync(_apiUrl + "card/v2/charge", content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ReponseMessage>(apiResponse);
                }
            }

            if (result.@object.Equals("error"))
            {
                return Json(new { status = "error", model = result  });
            }
            else
            {
                return Json(new { status = "success", model = result });
            }

            
        }

        public IActionResult ProcessResult(string ret, decimal AMT, string chg_id)
        {
            if (string.IsNullOrWhiteSpace(chg_id))
            {
                ViewBag.AppTitle = "Result";
                ViewBag.R = ret;
                ViewBag.AMT = AMT;

                return View();
            }
            else
            {
                var _model = SearchChrgeID(chg_id).Result;
                if (_model.@object != "error")
                {
                    if (_model.transaction_state == "Authorized")
                    {
                        //paid
                        bool _paid = _paidInvoice(_model.ref_1);
                        if (_paid)
                        {
                            ViewBag.AppTitle = "Result";
                            ViewBag.R = "Y";
                            ViewBag.AMT = _model.amount;
                            ViewBag.CUS = _model.ref_2;
                            return View();
                        }
                        else
                        {
                            goto onError;
                        }
                    }
                }
                else
                {
                    goto onError;
                }

            onError:
                ViewBag.AppTitle = "Result";
                ViewBag.R = "ไม่สามารถค้นหา Invoice No ได้";
                ViewBag.AMT = _model.amount;
                return View();
            }
        }

        [HttpPost]
        public virtual ActionResult Searching(string customer_no)
        {
            try
            {
                var result = _searching(customer_no);
                try
                {
                    if (result.Count() > 0)
                    {
                        return Json(new { status = "success", message = customer_no });
                    }
                    else
                    {
                        return Json(new { status = "error", message = "ไม่พบข้อมูลลูกค้า .. โปรดตรวจสอบอีกครั้ง" });
                    }
                }
                catch
                {
                    return Json(new { status = "error", message = "ข้อมูลไม่ถูกต้อง โปรดตรวจสอบอีกครั้ง !" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = ex.Message });
            }
        }

        [HttpPost]
        public async Task<ReponseInquiryMessage> SearchChrgeID(string chr_id)
        {
            _apiKey = Configuration.GetValue<string>("BaseData:SECKEY");
            string _apiUrl = Configuration.GetValue<string>("BaseData:APIUrl");
            ReponseInquiryMessage result = new ReponseInquiryMessage();
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("x-api-key", _apiKey);
                using (var response = await httpClient.GetAsync(_apiUrl + "card/v2/charge/" + chr_id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ReponseInquiryMessage>(apiResponse);
                }
            }
            return result;
        }
    }
}
