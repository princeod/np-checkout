﻿using Dapper;
using FrontEnd.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IConfiguration Configuration;
        private Uri baseUri;
        IDatabaseConnectionFactory _db;
        public HomeController(ILogger<HomeController> logger, IConfiguration _configuration, IDatabaseConnectionFactory _database)
        {
            _logger = logger;
            Configuration = _configuration;
            _db = _database;
        }
        private List<InvoiceData> _listInvoice()
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"Select * from tran_Invoice Order BY Id DESC";
            var _inv = conn.Query<InvoiceData>(query).ToList();
            return _inv;
        }
        public IActionResult ILINK()
        {
            var model = _listInvoice();
            ViewBag.AppTitle = "Invoice";
            return View(model);
        }
        public IActionResult CheckOut(string mid, string cardid, bool savecard, string token, decimal local_amount)
        {
            var rec = new ReceiveTransactionData()
            {
                mid = mid,
                cardid = cardid,
                secretKey = "skey_test_20972enANDYmuswf4xiZksGZ6pnGbHeoMUsU9",
                token = token,
                amount  = local_amount
            };
            return View(rec);
        }
        [HttpPost]
        public async Task<IActionResult> ProcessCharge(string model, string secret)
        {
            List<ChargeTransactionData> modeldata = JsonConvert.DeserializeObject<List<ChargeTransactionData>>(model);
            ChargeRedirectTransactionData result = new ChargeRedirectTransactionData();
            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(modeldata[0]), Encoding.UTF8, "application/json");
                httpClient.DefaultRequestHeaders.Add("x-api-key", secret);

                using (var response = await httpClient.PostAsync("https://dev-kpaymentgateway-services.kasikornbank.com/card/v2/charge", content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ChargeRedirectTransactionData>(apiResponse);
                }
            }
            return Json(new { status = "success", model = result });
        }

        public IActionResult ProcessResult()
        {
            ViewBag.AppTitle = "Result";
            ViewBag.R = "Y";
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
     
    }
}
