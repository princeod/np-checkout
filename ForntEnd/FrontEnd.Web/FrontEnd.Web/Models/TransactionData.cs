﻿using FrontEnd.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd.Web.Models
{
    public class ReceiveTransactionData
    {
        public string token { get; set; }
        public string objectId { get; set; }
        public string mid { get; set; }
        public string cardid { get; set; }
        public string secretKey { get; set; }
        public decimal amount { get; set; }

        public string cust_no { get; set; }
        public string cust_name { get; set; }
        public string invoice_no { get; set; }

        public string tel { get; set; }
        public  string email { get; set; }

        public string dcc_cur { get; set; }
    }

    public class ChargeTransactionData
    {
        public string token { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public string source_type { get; set; }
        public string mode { get; set; }
        public string reference_order { get; set; }
        public string ref_1 { get; set; }
        public string ref_2 { get; set; }
        public addData additional_data { get; set; }
        public dcc_data dcc_data { get; set; }

    }

 

    public class addData
    {
        public string mid { get; set; }
    }

    public class dcc_data
    {
        public string dcc_currency { get; set; }
    }

    public class ChargeRedirectTransactionData
    {
        public string id { get; set; }
        public string @object { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string transaction_state { get; set; }
        public source_detail source { get; set; }
        public string status { get; set; }
        public string reference_order { get; set; }
        public string description { get; set; }
        public string redirect_url { get; set; }
        public string approval_code { get; set; }
        public string ref_1 { get; set; }
        public string ref_2 { get; set; }
        public string ref_3 { get; set; }
        public string failure_code { get; set; }
        public string failure_message { get; set; }

    }

    public class ChargeRedirectTransactionResponse : BaseResponse
    {
        public ChargeRedirectTransactionData data { get; set; }
    }

    public class source_detail
    {
        public string id { get; set; }
        public string @object { get; set; }
        public string brand { get; set; }
        public string card_marking { get; set; }
        public string issuer_bank { get; set; }


    }
}


public class ReponseMessage
{
    public string @object { get; set; }
    public string code { get; set; }
    public string message { get; set; }
    public string redirect_url { get; set; }
    public string id { get; set; } // chage id
    public string transaction_state { get; set; }
}

public class ReponseInquiryMessage
{
    public string @object { get; set; }
    public string code { get; set; }
    public decimal amount { get; set; }
    public string currency { get; set; }
    public string message { get; set; }
    public string transaction_state { get; set; }
    public string id { get; set; } // chage id
    public source_detail source { get; set; }
    public string status { get; set; }
    public string reference_order { get; set; }
    public string description { get; set; }
    public string redirect_url { get; set; }
    public string approval_code { get; set; }
    public string ref_1 { get; set; }
    public string ref_2 { get; set; }
    public string ref_3 { get; set; }
    public string failure_code { get; set; }
    public string failure_message { get; set; }
}


public class BaseResponse
{
    public string[] message { get; set; }
    public bool iscompleted { get; set; }
}
