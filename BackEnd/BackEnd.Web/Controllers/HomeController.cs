﻿using BackEnd.Web.Models;
using Dapper;
using DotNet.RestApi.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IConfiguration Configuration;
        private Uri baseUri;
        IDatabaseConnectionFactory _db;
        public HomeController(ILogger<HomeController> logger, IConfiguration _configuration, IDatabaseConnectionFactory _database)
        {
            _logger = logger;
            Configuration = _configuration;
            _db = _database;
        }
        public IActionResult Index()
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"Select  *  from tran_Invoice";
            var _total = conn.Query<InvoiceData>(query).ToList();

            var _dash = new InvoiceDashboard();
            _dash.InvoiceTotal = _total.Sum(s => s.InvoiceAmount);
            _dash.PaidTotal = _total.Where(w => w.IsPaid).ToList().Sum(s => s.InvoiceAmount);
            _dash.InactiveTotal = _total.Where(w => !w.IsActive).ToList().Count();
            _dash.CustomerTotal = _total.GroupBy(g=> new { g.CustomerNo }).Select(s=> new {  s.Key.CustomerNo }).Count();

            ViewBag.InvoiceTotal = _dash.InvoiceTotal.ToString("#,0.00");
            ViewBag.InactiveTotal = _dash.InactiveTotal.ToString("#,0");
            ViewBag.CustomerTotal = _dash.CustomerTotal.ToString("#,0");
            ViewBag.PaidTotal = _dash.PaidTotal.ToString("#,0.00");
            return View();
        }
        public IActionResult IndexInvoice()
        {
            ViewBag.AppUrl = Configuration.GetSection("Api").GetSection("AppUrl").Value;
            var model = _listInvoice();
            return View(model);
        }


        public IActionResult CreateInvoice()
        {  
            return View();
        }
       
        [HttpPost]
        private async Task<String> LoginAction()
        {
            this.baseUri = new Uri(Configuration.GetSection("Api").GetSection("BaseUrl").Value);
            var resultLogin = new LoginResponse();
            var client = new RestClient(this.baseUri + "/token");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "password");
            request.AddParameter("username", "thannapan@interlink.co.th");
            request.AddParameter("password", "Ecom@2021");
            IRestResponse response = await client.ExecuteAsync(request);

            resultLogin = JsonConvert.DeserializeObject<LoginResponse>(response.Content);

            return resultLogin.access_token;
        }
        [HttpPost]
        public IActionResult CreateInvoice(InvoiceData model)
        {
            string _mess = "";
            long result = _addInvoice(model, ref _mess);
            if (result > 0)
            {
                TempData["SuccessMessages"] = "สร้างใบแจ้งหนี้เพื่อรับชำระสำเร็จ";

                //Email
                try
                {
                    SENDEMAIL(result);
                }
                catch
                {

                }

                //SMS
                try
                {
                    //
                    if (Configuration.GetSection("SMSConfig").GetSection("SMSActive").Value.ToString() != "0")
                    {
                        SENDSMS(result);
                    }
                }
                catch 
                {
                }
                return RedirectToAction("IndexInvoice");
            }
            else
            {
                if (!string.IsNullOrEmpty(_mess))
                {
                    TempData["ErrorMessages"] = "เกิดข้อผิดพลาด : " + _mess;
                }
                else
                {
                    TempData["ErrorMessages"] = "เกิดข้อผิดพลาดในการสร้างใบแจ้งหนี้ .. โปรดตรวจสอบ";
                }                
                return View(model);
            }
            
        }

        private long _addInvoice(InvoiceData model, ref string mess)
        {
            using var conn = _db.CreateConnectionAsync().Result;
            using var transaction = conn.BeginTransaction();
            var query = @"Select * from tran_Invoice where InvoiceNo = @InvoiceNo";
            var _inv = conn.Query<InvoiceData>(query, new { model.InvoiceNo }, transaction).FirstOrDefault();
            if (_inv != null)
            {
                mess = "ใบเลขใบแจ้งหนี้มีการสร้างการรับชำระแล้ว : " + model.InvoiceNo;
                transaction.Rollback();
                return 0;
            }
            else
            {
                //insert 

                var insertQuery = @"INSERT INTO [dbo].[tran_Invoice]
                                       ([CustomerNo]
                                       ,[CustomerName]
                                       ,[Telephone]
                                       ,[Email]
                                        ,[InvoiceNo]
                                        ,[InvoiceAmount]
                                        ,[IsActive],[IsPaid])
                                 VALUES
                                       (@CustomerNo
                                       ,@CustomerName
                                       ,@Telephone
                                       ,@Email
                                        ,@InvoiceNo
                                        ,@InvoiceAmount,@IsActive, 0)";
                conn.Execute(insertQuery, model, transaction);
                long HeaderId = Convert.ToInt64(conn.ExecuteScalar<object>("SELECT @@IDENTITY", null, transaction));
                //_inv = conn.Query<InvoiceData>(query, new { model.InvoiceNo }).FirstOrDefault();
                transaction.Commit();
                return HeaderId;
            }
        }
        private bool _deleteInvoice(long Id, ref string mess)
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"Select * from tran_Invoice where Id = @Id and IsPaid = 0";
            var _inv = conn.Query<InvoiceData>(query, new { Id }).FirstOrDefault();
            if (_inv == null)
            {
                mess = "ไม่พบข้อมูลใบแจ้งหนี้นี้ หรือใบแจ้งหนี้มีการชำระเงินแล้ว ไม่สามารถลบได้";
                return false;
            }
            else
            {
                //delete
                var insertQuery = @"delete from tran_invoice where Id = @Id";
                conn.Execute(insertQuery, new { Id });
                return true;
            }
        }
        private bool _activeInvoice(long Id, int type, ref string mess)
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"Select * from tran_Invoice where Id = @Id";
            var _inv = conn.Query<InvoiceData>(query, new { Id }).FirstOrDefault();
            if (_inv == null)
            {
                mess = "ไม่พบข้อมูลใบแจ้งหนี้นี้";
                return false;
            }
            else
            {
                //delete
                var insertQuery = @"update tran_invoice set IsActive = @type where Id = @Id";
                conn.Execute(insertQuery, new { Id, type });
                return true;
            }
        }
        private List<InvoiceData> _listInvoice()
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"Select * from tran_Invoice Order BY Id DESC";
            var _inv = conn.Query<InvoiceData>(query).ToList();
            return _inv;
        }

        [HttpPost]
        public virtual ActionResult getBalance(string invoice_no, string customer_no)
        {
            try
            {
                this.baseUri = new Uri(Configuration.GetSection("Api").GetSection("BaseUrl").Value);
                string _token = LoginAction().Result;
                var client = new RestClient(this.baseUri + "/api/NAV/CustBalance_Remaining_Amount?CustomerNo=" + customer_no + "&DocumentType=2&DocumentNo=" + invoice_no);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Bearer " + _token);
                IRestResponse response = client.Execute(request);

                try
                {
                    return Json(new { status = "success", message = decimal.Parse(response.Content) });
                }
                catch
                {
                    return Json(new { status = "error", message = "ข้อมูลไม่ถูกต้อง โปรดตรวจสอบอีกครั้ง !" });
                }            
            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = ex.Message });
            }
        }

        [HttpPost]
        public virtual ActionResult Delete(long Id)
        {
            try
            {
                string _mess = "";
                bool result = _deleteInvoice(Id, ref _mess);
                try
                {
                    if (string.IsNullOrEmpty(_mess)) {
                        return Json(new { status = "success", message = "" });
                    }
                    else
                    {
                        return Json(new { status = "error", message = _mess });
                    }
                }
                catch
                {
                    return Json(new { status = "error", message = "ข้อมูลไม่ถูกต้อง โปรดตรวจสอบอีกครั้ง !" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = ex.Message });
            }
        }

        [HttpPost]
        public virtual ActionResult Active(long Id, int type)
        {
            try
            {
                string _mess = "";
                bool result = _activeInvoice(Id, type,ref _mess);
                try
                {
                    if (string.IsNullOrEmpty(_mess))
                    {
                        return Json(new { status = "success", message = "" });
                    }
                    else
                    {
                        return Json(new { status = "error", message = _mess });
                    }
                }
                catch
                {
                    return Json(new { status = "error", message = "ข้อมูลไม่ถูกต้อง โปรดตรวจสอบอีกครั้ง !" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = ex.Message });
            }
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public virtual JsonResult SENDEMAIL(long id)
        {
            try
            {
                string _mess = "";
               
                bool result = _sendEmail(id, ref _mess);
                try
                {
                    if (result)
                    {
                        return Json(new { status = "success", message = "" });
                    }
                    else
                    {
                        return Json(new { status = "error", message = _mess });
                    }
                }
                catch
                {
                    return Json(new { status = "error", message = "ข้อมูลไม่ถูกต้อง โปรดตรวจสอบอีกครั้ง !" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = ex.Message });
            }
        }
        public virtual JsonResult SENDSMS(long id)
        {
            try
            {
                string _mess = "";

                bool result = _sendSMS(id, ref _mess);
                try
                {
                    if (result)
                    {
                        return Json(new { status = "success", message = "" });
                    }
                    else
                    {
                        return Json(new { status = "error", message = _mess });
                    }
                }
                catch
                {
                    return Json(new { status = "error", message = "ข้อมูลไม่ถูกต้อง โปรดตรวจสอบอีกครั้ง !" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = ex.Message });
            }
        }
        private bool _sendSMS(long Id, ref string mess)
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"Select * from tran_Invoice where Id = @Id";
            var _inv = conn.Query<InvoiceData>(query, new { Id }).FirstOrDefault();
            if (_inv == null)
            {
                mess = "ไม่พบข้อมูลใบแจ้งหนี้นี้ ไม่สามารถส่งข้อมูล SMS ได้ !";
                return false;
            }
            else
            {

                if (String.IsNullOrWhiteSpace(_inv.Telephone))
                {
                    mess = "ไม่พบข้อมูล เบอร์โทรศัพท์ลูกค้า ..ไม่สามารถส่งข้อมูล SMS ได้ !";
                    return false;
                }

                string _smsKey = Configuration.GetSection("SMSConfig").GetSection("SMSAPIKey").Value;
                string _smsSecret = Configuration.GetSection("SMSConfig").GetSection("SMSAPISecret").Value;
                string _smsUrl = Configuration.GetSection("SMSConfig").GetSection("SMSAPIUrl").Value;
                int _smsActive = int.Parse(Configuration.GetSection("SMSConfig").GetSection("SMSActive").Value);

                string _link = Configuration.GetSection("Api").GetSection("AppUrl").Value + "CheckOut/Index?customer=" + _inv.CustomerNo;
                var result = new ReturnMessage();
                string _mess = "โปรดชำระเงินค่าสินค้าให้ ILINK ตาม Order Number {0} (เลขที่ใบสั่งส่ง หรือ เลขที่ Invoice)\n กรุณาคลิ๊ก LINK : {1}";
                string _body = String.Format(_mess,
                  _inv.InvoiceNo, _link);
                try
                {
                    var client = new RestClient(_smsUrl);
                    client.Authenticator = CreateBasicAuth(_smsKey, _smsSecret);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddJsonBody(new { msisdn = _inv.Telephone, message = _body, sender = "MySMS", scheduled_delivery = "", force = "" });
                   
                    IRestResponse response = client.Execute(request);
                    result.iscompleted = response.IsSuccessful; result.message.Add(response.Content);
                    //Console.WriteLine(response.Content);
                    mess = result.message[0];
                    return result.iscompleted;
                }
                catch (Exception ex)
                {
                    result.iscompleted = false;
                    result.message.Add(ex.Message);
                    mess = result.message[0];
                    return result.iscompleted;
                }
           
            }
        }
        private static HttpBasicAuthenticator CreateBasicAuth(string apiKey, string secretKey)
        {
            if (String.IsNullOrEmpty(apiKey) || String.IsNullOrEmpty(secretKey))
            {
                throw new ArgumentException("Api Ket or Secret Key is missing");
            }
            var authen = new HttpBasicAuthenticator(apiKey, secretKey);

            return authen;
        }

        private bool _sendEmail(long Id, ref string mess)
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"Select * from tran_Invoice where Id = @Id";
            var _inv = conn.Query<InvoiceData>(query, new { Id }).FirstOrDefault();
            if (_inv == null)
            {
                mess = "ไม่พบข้อมูลใบแจ้งหนี้นี้ ไม่สามารถส่งข้อมูล Email ได้ !";
                return false;
            }
            else
            {

                if (String.IsNullOrWhiteSpace(_inv.Email))
                {
                    mess = "ไม่พบข้อมูล Email ลูกค้า ..ไม่สามารถส่งข้อมูล Email ได้ !";
                    return false;
                } 

                string _emailfrom = Configuration.GetSection("EmailConfig").GetSection("EMAIL_NAME").Value;
                string _emailword = Configuration.GetSection("EmailConfig").GetSection("EMAIL_WORD").Value;
                string _emailhost = Configuration.GetSection("EmailConfig").GetSection("EMAIL_HOST").Value;
                int _emailport = int.Parse(Configuration.GetSection("EmailConfig").GetSection("EMAIL_PORT").Value);

                string _link = Configuration.GetSection("Api").GetSection("AppUrl").Value + "CheckOut/Index?customer=" + _inv.CustomerNo;
                //send email
                MailMessage mail = new MailMessage();
                mail.To.Add(_inv.Email);
                mail.From = new MailAddress(_emailfrom);
                mail.Subject = "[ Offline CheckOut - ILINK ] : ระบบรับชำระเงินแบบ Offline";
                string Body = "เรียน คุณลูกค้า {0}, <br><br>โปรดชำระเงินค่าสินค้าให้ ILINK ตาม Order Number <b>{1}</b> (เลขที่ใบสั่งส่ง หรือ เลขที่ Invoice) " +
                    "<br><br> กรุณาคลิ๊ก LINK : {2} <br><br>-------------------------<br>ขอแสดงความนับถือ<br>ILINK Offline CheckOut System";
                mail.Body = String.Format(Body,
                    "("+_inv.CustomerNo + ") "+_inv.CustomerName, _inv.InvoiceNo, _link);
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = _emailhost;
                smtp.Port = _emailport;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(_emailfrom, _emailword);
                smtp.EnableSsl = false;

                smtp.Send(mail);
                return true;
            }
        }
        private bool _sendEmailLocal(long Id, ref string mess)
        {
            using var conn = _db.CreateConnectionAsync().Result;
            var query = @"Select * from tran_Invoice where Id = @Id";
            var _inv = conn.Query<InvoiceData>(query, new { Id }).FirstOrDefault();
            if (_inv == null)
            {
                mess = "ไม่พบข้อมูลใบแจ้งหนี้นี้ ไม่สามารถส่งข้อมูล Email ได้ !";
                return false;
            }
            else
            {

                if (String.IsNullOrWhiteSpace(_inv.Email))
                {
                    mess = "ไม่พบข้อมูล Email ลูกค้า ..ไม่สามารถส่งข้อมูล Email ได้ !";
                    return false;
                }

                string _emailfrom = Configuration.GetSection("EmailConfig").GetSection("EMAIL_NAME").Value;
                string _emailword = Configuration.GetSection("EmailConfig").GetSection("EMAIL_WORD").Value;
                string _emailhost = Configuration.GetSection("EmailConfig").GetSection("EMAIL_HOST").Value;
                int _emailport = int.Parse(Configuration.GetSection("EmailConfig").GetSection("EMAIL_PORT").Value);
                string _emailpay = Configuration.GetSection("EmailConfig").GetSection("EMAIL_PAY").Value;
                string _link = Configuration.GetSection("Api").GetSection("AppUrl").Value + "CheckOut/Index?customer=" + _inv.CustomerNo;
                //send email
                MailMessage mail = new MailMessage();
                mail.To.Add(_emailpay);
                mail.From = new MailAddress(_emailfrom);
                mail.Subject = "[ Offline CheckOut - ILINK ] : ระบบรับชำระเงินแบบ Offline";
                string Body = "เรียน Payment Team, <br><br>Order Number <b>{0}</b> (เลขที่ใบสั่งส่ง หรือ เลขที่ Invoice) <br><br>โปรดเตรียมเอกสารและส่งสินค้าให้ลูกค้า ผ่านการรับชำระเงินด้วยระบบ Payment Gateway " + 
                    "<br><br>ขอแสดงความนับถือ<br>ILINK Offline CheckOut System";
                mail.Body = String.Format(Body,
                   _inv.InvoiceNo);
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = _emailhost;
                smtp.Port = _emailport;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(_emailfrom, _emailword);
                smtp.EnableSsl = false;

                smtp.Send(mail);
                return true;
            }
        }
    }
}
