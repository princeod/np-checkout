﻿using BackEnd.Web.Models;
using DotNet.RestApi.Client;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BackEnd.Web.Services
{
    public class InvoiceService
    {
        private IConfiguration config;
        private Uri baseUri;
        public InvoiceService(IConfiguration _configuration)
        {
            config = _configuration;
        }

        public ReturnMessage CreateInvoice(InvoiceData model)
        {
            var result = new ReturnMessage();
            try
            {
                this.baseUri = new Uri(config.GetSection("Api").GetSection("BaseUrl").Value);
                var loginData = new LoginData()
                {
                    grant_type = "password",
                    username = "thannapan@interlink.co.th",
                    password = "Ecom@2021"
                };
                RestApiClient clientLogin = new RestApiClient(baseUri);
                HttpResponseMessage response = clientLogin.SendJsonRequest(HttpMethod.Post, new Uri("/token", UriKind.Relative), loginData).Result;

                LoginResponse respObj = response.DeseriaseJsonResponse<LoginResponse>();

                string _token = respObj.access_token;
                return result;
            }
            catch (Exception ex)
            {
                result.iscompleted = false;
                result.message.Add(ex.Message);
                return result;
            }

        }

    }
}
