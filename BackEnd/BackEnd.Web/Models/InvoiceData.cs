﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Web.Models
{
    public class InvoiceData
    {
        public long Id { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceNo { get; set; }

        public decimal InvoiceAmount { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public DateTime LogDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsPaid { get; set; }
    }

    public class LoginData
    {
        public string grant_type { get; set; }
        public string username { get; set; }
        public string password { get; set; }

    }
    public class LoginResponse
    {
        public string access_token { get; set; }
    }
    public class ReturnMessage
    {
        private List<String> _message = new List<String>();
        public List<String> message
        {
            get
            {
                return _message;
            }
            set
            {
                if (value == null)
                    _message = new List<String>();
                else
                    _message = value;
            }
        }

        public bool iscompleted { get; set; }
    }
    public class ReturnObject<T> : ReturnMessage
    {
        public T data { get; set; }
    }
    public class ReturnList<T> : ReturnObject<List<T>>
    {
        //public Int32 totalitem { get; set; }
        //public Int32 TotalPage { get; set; }
    }
}

public class InvoiceDashboard
{
    public decimal InvoiceTotal { get; set; }
    public int CustomerTotal { get; set; }
    public decimal PaidTotal { get; set; }
    public int InactiveTotal { get; set; }

}